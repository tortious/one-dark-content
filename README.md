# One Dark Content
This is an optional extra for the [One Dark Theme](https://gitlab.come/AtomRidge/one-dark-theme), which will theme several internal pages, unlike the WebExtension theme on its own.

Currently this will theme:
* `about:preferences`
* `about:addons`
* `about:performance`
* `about:blank`
* `about:config` (redesigned variant only)

## Installation
1. Open `about:support` in Firefox
2. You will see an row titled `Profile Directory`. Open this directory.
3. Create a folder here named `chrome`
4. Now clone the repository onto your computer.
5. Copy the `usercontent.css` file inside the repository into the `chrome` folder.
6. Restart Firefox for the changes to take effect

## Updating:
Assuming you haven't deleted the cloned repository, run `git pull` from the directory periodically. Then replace the old `userContent.css` file with the new one which should now appear.

If you've deleted it, or you don't have Git installed, then reclone the repo again, and replace the old `userContent.css` with the new one.

## Additional Information:
Currently, `about:config` theming is commented out to avoid breaking the pre-redesign variant. If you currently have the redesigned variant, you can uncomment the code in `userContent.css` which affects `about:config` to enable it.

Feel free to leave a feature request or bug report if you want.